var app = angular.module("weatherApp", []);
app.controller('mainController', function($scope,$http, $q) {
    // 5 European cities
    $scope.eurocities = ['Barcelona','Milan', 'Venice' ,'Rome' ,'Valencia'];

    // Api Key for open Weather Map
    var apiKey = 'dbdaecaeb7ff7b148c5cd227b1a89e73';

    var url = 'http://api.openweathermap.org/data/2.5/weather?q=';

    $scope.infoDetails = [];

    //Initially sea-level not to be shown
    $scope.showData = false;

    $scope.fullLinks = $scope.eurocities.map(function(eurocity) {
        return url + eurocity + '+&APPID='+apiKey+'&units=metric';
    });

    // Get URL for the basic data of the city by query the city name in URL
    $q.all($scope.fullLinks.map(function(url) {
      return $http.get(url).then(function(response) {
          var sunridate = response.data.sys.sunrise;
          var sunsetdate = response.data.sys.sunset;
          $scope.infoDetails.push({
                euroId: response.data.id,
                city : response.data.name,
                temp: response.data.main.temp_max,
                sunrise : formatTime(sunridate),
                sunset :formatTime(sunsetdate)
            });
        });
    })).then (function() {
        console.log($scope.infoDetails);
    });

    // Extends the city by providing sealevel upto 5 weekdays at 9:00 AM
    $scope.showSeaLevel = function (euroId) {
        //Sea Level Data based on weekdays populated based on time , day and sea_level from API
        $scope.showData = true;
        $scope.seaLevelData = [];
        $http.get('http://api.openweathermap.org/data/2.5/forecast?id='+euroId+'&APPID='+apiKey+'&units=metrics')
            .then (function (response) {
                var list = response.data.list;
                $q.all(response.data.list.map(function(list) {
                    var time = formatAMPM(list.dt_txt);
                    if(time === '9:00 AM') {
                        $scope.seaLevelData.push({
                            time: time,
                            day : getDay(list.dt_txt),
                            seaLevel:list.main.sea_level
                        });
                    }
                })).then (function () {
                    console.log($scope.seaLevelData);
                })
        });
    };



    // Convert UNIX time format to human readable time format
    function formatTime(timestamp) {
      var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
   	    hh = d.getHours(),
   		h = hh,
   		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
   		ampm = 'AM',
   		time;

     	if (hh > 12) {
     		h = hh - 12;
     		ampm = 'PM';
     	} else if (hh === 12) {
     		h = 12;
     		ampm = 'PM';
     	} else if (hh == 0) {
     		h = 12;
     	}
     	// 8:35 AM
     	time = h + ':' + min + ' ' + ampm;

     	return time;
    }

    function getDay(date) {
        var xx = new Date(date);
        return xx.toString().split(' ')[0];
    }

    function formatAMPM(date) {
        date = new Date(date);  
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
   }

});
